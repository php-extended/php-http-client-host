<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-host library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\HttpClient\HostClient;
use PhpExtended\HttpMessage\Request;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\Uri;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class HostClientClient implements ClientInterface
{
	
	public $request;
	
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		$this->request = $request;
		
		return new Response();
	}
	
}

/**
 * HostClientTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\HostClient
 *
 * @internal
 *
 * @small
 */
class HostClientTest extends TestCase
{
	
	/**
	 * The client to help.
	 * 
	 * @var HostClientClient
	 */
	protected HostClientClient $_client;
	
	/**
	 * The object to test.
	 * 
	 * @var HostClient
	 */
	protected HostClient $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testAddHeaderHttp() : void
	{
		$request = new Request();
		$request = $request->withUri(new Uri('http', null, new Domain(['domain', 'com'])));
		$response = $this->_object->sendRequest($request);
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertEquals('domain.com', $this->_client->request->getHeaderLine('Host'));
	}
	
	public function testAddHeaderHttpPort() : void
	{
		$request = new Request();
		$request = $request->withUri(new Uri('http', null, new Domain(['domain', 'com']), 8080));
		$response = $this->_object->sendRequest($request);
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertEquals('domain.com:8080', $this->_client->request->getHeaderLine('Host'));
	}
	
	public function testAddHeaderHttps() : void
	{
		$request = new Request();
		$request = $request->withUri(new Uri('https', null, new Domain(['domain', 'com'])));
		$response = $this->_object->sendRequest($request);
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertEquals('domain.com', $this->_client->request->getHeaderLine('Host'));
	}
	
	public function testAddHeaderHttpsPort() : void
	{
		$request = new Request();
		$request = $request->withUri(new Uri('https', null, new Domain(['domain', 'com']), 8081));
		$response = $this->_object->sendRequest($request);
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertEquals('domain.com:8081', $this->_client->request->getHeaderLine('Host'));
	}
	
	public function testDoNothing() : void
	{
		$request = new Request();
		$request = $request->withUri(new Uri('http', null, new Domain(['domain', 'com'])));
		$request = $request->withAddedHeader('Host', 'foo.bar');
		$response = $this->_object->sendRequest($request);
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertEquals('foo.bar', $this->_client->request->getHeaderLine('Host'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_client = new HostClientClient();
		
		$this->_object = new HostClient($this->_client);
	}
	
}
